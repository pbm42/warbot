package warbot.chevri_t;

import java.util.HashMap;
import java.util.HashSet;

import warbot.chevri_t.Mission.MissionType;

import madkit.kernel.AgentAddress;

@SuppressWarnings("serial")
public class Affectation extends HashMap<String, HashSet<AgentAddress>>
{
  public Affectation() {
    super();

    for(Mission.MissionType mt : Mission.MissionType.values())
      super.put(mt.toString(), new HashSet<AgentAddress>());
  }

  public void put(String goal, AgentAddress agent) {
    Mission.MissionType mt = MissionType.valueOf(goal);

    get(mt.toString()).add(agent);
  }

  public void remove(AgentAddress agent) {
    for (HashSet<AgentAddress> hs : values())
      hs.remove(agent);
  }

  public int size(Mission.MissionType mt) {
    return get(mt.toString()).size();
  }
}
