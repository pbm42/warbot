package warbot.chevri_t;

public class Ally
{
  private WBPoint pos;
  private double  radius;

  public Ally(WBPoint position, double radius) {
    pos = position;
    this.radius = radius;
  }

  /**
   * @return the position
   */
  public WBPoint getPosition() {
    return pos;
  }

  /**
   * @return the radius
   */
  public double getRadius() {
    return radius;
  }

}
