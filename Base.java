package warbot.chevri_t;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import madkit.kernel.AgentAddress;
import warbot.chevri_t.Mission.MissionType;
import warbot.chevri_t.MobileFoe.Type;
import warbot.kernel.Food;
import warbot.kernel.Percept;
import warbot.kernel.WarbotMessage;

/**
 * @author chevri_t
 * 
 */
@SuppressWarnings("serial")
public final class Base extends Robot
{
  private class AAetWB
  {
    public AgentAddress aa;
    public WBPoint      pos;

    public AAetWB(AgentAddress a, double x, double y) {
      aa = a;
      pos = new WBPoint(x, y);
    }
  };

  private final double                  ratioRecExpl = 2;
  private final double                  ratioDefAtta = 1.5;
  private final String                  roleName     = "Base";
  private int                           nbEnnemis    = 0;
  private int                           nbNakama     = 0;
  private Boolean                       estDecouvert = false;
  private Boolean                       createLaunch = false;
  private Boolean                       createExplor = false;
  private AgentAddress                  refPos       = null;
  private WBPoint                       posToRef     = null;
  private final TreeMap<String, AAetWB> bases        = new TreeMap<String, AAetWB>();
  private final Affectation             affect       = new Affectation();
  protected MobileFoes                  foes         = new MobileFoes();

  @Override
  public void activate() {
    super.activate();
    requestRole(groupName, roleName, null);
    bases.put(getAddress().getLocalID(), new AAetWB(getAddress(), 0, 0));
    Say("My local ID is", getAddress().getLocalID());
  }

  /*
   * (non-Javadoc)
   * @see warbot.kernel.Brain#doIt()
   */
  @Override
  public void doIt() {
    super.doIt();

    if (refPos == null)
      if (compteur == 1) {
        Say("Request MAster");
        broadcast(groupName, "Base", Cst.Msg.masterReq);
      } else if (compteur > 2) {
        refPos = bases.firstEntry().getValue().aa;
        Say("Master : ", refPos.toString());
        posToRef = bases.firstEntry().getValue().pos;
      }

    nbEnnemis = 0;
    nbNakama = 0;
    Food food = null;
    Percept[] perception = getPercepts();

    for (Percept element : perception) {
      String elmType = element.getPerceptType();
      Boolean foe = !element.getTeam().equals(getTeam());

      if (elmType.equals("Explorer") && foe) {
        int perceptRad = foe ? Cst.DR.ExplorerDR : 20;
        foes.put(element, posToRef, foe, perceptRad);
        setUserMessage("Explorateur ennemis !");
        foeNear = true;
        if (element.getDistance() > Cst.DR.ExplorerDR) {
          if (!estAttaquee)
            Messenger(groupName, "Launcher", Cst.Msg.Goal.help,
                Cst.Priority.Low);
        } else {
          estDecouvert = true;
          nbEnnemis++;
        }
      } else if (elmType.equals("RocketLauncher") && foe) {
        int perceptRad = foe ? Cst.DR.LauncherDR : 20;
        foes.put(element, posToRef, foe, perceptRad);
        setUserMessage("RocketLauncher ennemis !");
        foeNear = true;
        if (element.getDistance() > Cst.DR.LauncherDR) {
          if (!estAttaquee)
            Messenger(groupName, "Launcher", Cst.Msg.Goal.help,
                estDecouvert ? Cst.Priority.High : Cst.Priority.Medium);
        } else {
          foeAttackPos = toWBPoint(element);
          estDecouvert = true;
          nbEnnemis++;
        }
      } else if (elmType.equals("RocketLauncher") && !foe)
        nbNakama++;
      else if (elmType.equals("Food"))
        if (element.getDistance() < 2)
          food = (Food)element;
    }

    if (foeNear || createExplor || createLaunch)
      if (food != null) {
        if (getEnergyLevel() >= getMaximumEnergy())
          createLaunch = true;
        eat(food);
      }

    if (estAttaquee) {
      if (nbEnnemis > nbNakama)
        createAgent("PBMMissileLauncher");
      WBPoint pos = foeAttackPos == null ? posToRef : foeAttackPos;
      String[] args = new String[] { Cst.Priority.Absolute,
          Double.toString(pos.x), Double.toString(pos.y) };
      Messenger(groupName, "Launcher", Cst.Msg.Goal.help, args);
      Messenger(groupName, "Base", Cst.Msg.Goal.help, Cst.Priority.Absolute);
    } else if (createLaunch)
      createAgent("PBMMissileLauncher");
    else if (createExplor)
      createAgent("PBMDetector");

    if (!foeNear && !estAttaquee
        && messages.has(groupName, "Launcher", Cst.Msg.Goal.help)) {
      Messenger(groupName, "Launcher", Cst.Msg.cancel, Cst.Msg.Goal.help);
      Messenger(groupName, "Base", Cst.Msg.cancel, Cst.Msg.Goal.help);
    }

    ArrayList<MobileFoe> toDel = new ArrayList<MobileFoe>();

    for (MobileFoe set : foes) {
      if (set.getTick() > 20 || !set.getRefreshed()
          && set.getDistance(posToRef) < Cst.DR.BaseDR - 5)
        toDel.add(set);
      set.setRefreshed(false);
    }

    for (MobileFoe key : toDel) {
      broadcast(groupName, "mobile", Cst.Msg.delFoe, key.toStrings());
      foes.remove(key);
    }

    for (MobileFoe m : foes)
      if (m.IsFoe() && !m.getType().equals(Type.Rocket) && m.IsPercepted())
        broadcast(groupName, "mobile", Cst.Msg.foe, m.toStrings());
  }

  @Override
  public void end() {
    broadcast(groupName, "mobile", Cst.Msg.dead, String.valueOf(nbEnnemis));
    broadcast(groupName, "Base", Cst.Msg.dead);
  }

  private void AffectWB(AgentAddress sender, String role) {
    if (role.equals("Explorer")) {
      int nbExplo = affect.size(MissionType.EXPLORE);
      int nbRecolt = affect.size(MissionType.RECOLT);

      if (nbRecolt > ratioRecExpl * nbExplo) {
        affect.put(MissionType.EXPLORE.toString(), sender);
        Say("Affect", sender.getLocalID(), "to Exploration");
        send(sender, Cst.Msg.goalRep, new String[] { Cst.Priority.Low,
            Cst.Msg.Goal.explore });
      } else {
        affect.put(MissionType.RECOLT.toString(), sender);
        Say("Affect", sender.getLocalID(), "to Recolt");
        send(sender, Cst.Msg.goalRep, new String[] { Cst.Priority.Low,
            Cst.Msg.Goal.recolt });
      }
    } else if (role.equals("Launcher")) {
      int nbDefend = affect.size(MissionType.HELP);
      int nbAttack = affect.size(MissionType.ATTACK);

      if (nbAttack > ratioDefAtta * nbDefend) {
        affect.put(MissionType.HELP.toString(), sender);
        Say("Affect", sender.getLocalID(), "to Defense");
        send(sender, Cst.Msg.goalRep, new String[] { Cst.Priority.Low,
            Cst.Msg.Goal.help });
      } else {
        affect.put(MissionType.ATTACK.toString(), sender);
        Say("Affect", sender.getLocalID(), "to Attack");
        send(sender, Cst.Msg.goalRep, new String[] { Cst.Priority.Medium,
            Cst.Msg.Goal.attack });
      }
    }

  }

  @Override
  protected void CheckMessages() {
    WarbotMessage msg = null;
    ArrayList<KnightCandidate> candidates = new ArrayList<KnightCandidate>();

    while (!isMessageBoxEmpty()) {
      msg = readMessage();
      if (msg.getSender() == msg.getReceiver())
        continue;

      if (msg.getAct().equals(Cst.Msg.helpRT)) {
        AgentAddress k_aa = msg.getSender();
        double k_x = msg.getFromX();
        double k_y = msg.getFromY();
        int k_hp = Integer.parseInt(msg.getArg1());
        double k_score = DefinePriority(k_x, k_y, k_hp);
        KnightCandidate k = new KnightCandidate(k_aa, k_score);
        candidates.add(k);
      } else if (msg.getAct().equals(Cst.Msg.Goal.help))
        missions.add(new Mission(msg.getArg1(), msg.getSender(),
            Cst.Msg.Goal.help, new WBPoint(msg.getFromX(), msg.getFromY())));
      else if (msg.getAct().equals(Cst.Msg.dead)) {
        missions.remove(msg.getSender());
        affect.remove(msg.getSender());
      } else if (msg.getAct().equals(Cst.Msg.cancel))
        missions.remove(msg.getSender(), msg.getArg1());
      else if (msg.getAct().equals(Cst.Msg.goalReq))
        AffectWB(msg.getSender(), msg.getArg1());
      else if (msg.getAct().equals(Cst.Msg.ko)) {
        if (msg.getArg1().equals(Cst.Msg.goalRep))
          affect.remove(msg.getSender());
      } else if (msg.getAct().equals(Cst.Msg.masterRep))
        bases.put(msg.getSender().getLocalID(), new AAetWB(msg.getSender(),
            -msg.getFromX(), -msg.getFromY()));
      else if (msg.getAct().equals(Cst.Msg.masterReq))
        send(msg.getSender(), Cst.Msg.masterRep);
      else if (msg.getAct().equals(Cst.Msg.posReq) && posToRef != null) {
        Say("Answer pos Req");
        send(msg.getSender(), Cst.Msg.posRep, posToRef.toStrings());
      } else if (msg.getAct().equals(Cst.Msg.ping))
        send(msg.getSender(), Cst.Msg.pong, "Base");
      else if (msg.getAct().equals(Cst.Msg.refreshMission)) {
        int nbDefend = affect.size(MissionType.HELP);
        int nbAttack = affect.size(MissionType.ATTACK);

        if (msg.getArg1().equals(Cst.Msg.Goal.help))
          if (nbAttack <= ratioDefAtta * (nbDefend - 1)) {
            send(msg.getSender(), Cst.Msg.cancel, Cst.Msg.Goal.help);
            affect.remove(msg.getSender());
          }
      }
    }
    Collections.sort(candidates);
    int i = 0;
    for (; i < nbEnnemis - nbNakama && i < candidates.size(); ++i)
      send(candidates.get(i).getAgentAddr(), Cst.Msg.ok,
          new String[] { Cst.Msg.Goal.help });
    for (; i < candidates.size(); ++i)
      send(candidates.get(i).getAgentAddr(), Cst.Msg.ko,
          new String[] { Cst.Msg.Goal.help });
  }

  @Override
  protected void ManageMissions() {
    createLaunch = false;
    createExplor = false;

    // Collections.sort(missions);
    if (affect.size(MissionType.ATTACK) == 0 && compteur > 15)
      createLaunch = true;
    if (affect.size(MissionType.EXPLORE) == 0 && compteur > 15) {
      createExplor = true;
      for (AgentAddress t : affect.get(MissionType.ATTACK.toString())) {
        if (affect.size(MissionType.EXPLORE) > 1)
          break;
        affect.put(MissionType.EXPLORE.toString(), t);
        send(t, Cst.Msg.Goal.explore);
      }
    }

    for (Mission m : missions)
      switch (m.getMissionType()) {
      case HELP:
        createLaunch = true;
      default:
        break;
      }
  }

  protected WBPoint toWBPoint(Percept p) {
    if (posToRef == null)
      return null;
    return new WBPoint(p.getX() + posToRef.getX(), p.getY() + posToRef.getY());
  }
}
