package warbot.chevri_t;

public final class Cst
{
  public final static class CD
  {
    public final static double ExplorerCD  = 1;
    public final static double LauncherCD  = 0.25;
    public final static int    RocketCD    = 2;
    public final static int    RocketMaxCD = 200;
  };

  public final class DR
  {
    public final static int ExplorerDR = 130;
    public final static int BaseDR     = 200;
    public final static int LauncherDR = 80;
  };

  public final class Msg
  {
    public final class Goal
    {
      public final static String help    = "HELP";
      public final static String attack  = "ATTACK";
      public final static String explore = "EXPLORE";
      public final static String recolt  = "RECOLT";

    }

    public final static String maj            = "UPDATE";
    public final static String dead           = "DEAD";
    public final static String ping           = "PING";
    public final static String pong           = "PONG";
    public final static String ko             = "KO";
    public final static String ok             = "OK";
    public final static String cancel         = "CANCEL";
    public final static String helpRT         = "HELP-RETURN";
    public final static String foodRT         = "FOOD-RETURN";
    public final static String attackRT       = "ATTACK-RETURN";
    public final static String masterReq      = "MASTER-REQU";
    public final static String masterRep      = "MASTER-REPL";
    public final static String posReq         = "POS-REQU";
    public final static String posRep         = "POS-REPL";
    public final static String foe            = "FOE";
    public final static String delFoe         = "DEAD-FOE";
    public final static String IntPoint       = "INTEREST-POINT";
    public final static String delIntPoint    = "DEAD-INTEREST-POINT";
    public final static String goalReq        = "GOAL-REQU";
    public final static String goalRep        = "GOAL-REP";
    public final static String refreshMission = "REFRESH-MISSION";
  }

  public final static class Priority
  {
    public final static String Absolute = "0";
    public final static String High     = "1";
    public final static String Medium   = "2";
    public final static String Low      = "3";

    public static int compareTo(String priority1, String priority2) {
      Integer one = Integer.parseInt(priority1);
      Integer two = Integer.parseInt(priority2);
      int result = one.compareTo(two);

      return result;
    }
  };
}
