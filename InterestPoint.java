package warbot.chevri_t;

@SuppressWarnings("serial")
public class InterestPoint extends WBPoint
{

  public enum Type {
    Base("Base"), Food("Food"), Pantry("Pantry");

    private final String text;

    private Type(final String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  private boolean    isNew       = true;
  private boolean    isRefreshed = true;

  private final Type type;

  public InterestPoint(double x, double y, Type type) {
    super(x, y);
    this.type = type;
  }

  public InterestPoint(WBPoint point, Type type) {
    super(point);
    this.type = type;
  }

  /**
   * @return the type
   */
  public Type getType() {
    return type;
  }

  /**
   * @return the isNew
   */
  public boolean isNew() {
    return isNew;
  }

  public boolean isRefreshed() {
    return isRefreshed;
  }

  public void setOld() {
    isNew = false;
  }

  public void setRefreshed(boolean is) {
    isRefreshed = is;
  }

  @Override
  public String[] toStrings() {
    return new String[] { type.toString(), java.lang.Double.toString(x),
        java.lang.Double.toString(y) };
  }
}
