package warbot.chevri_t;

import java.util.ArrayList;

import warbot.chevri_t.InterestPoint.Type;
import warbot.kernel.WarbotMessage;

@SuppressWarnings("serial")
public class InterestPoints extends ArrayList<InterestPoint>
{
  private double perceptRadius = 1;

  /*
   * (non-Javadoc)
   * @see java.util.ArrayList#add(java.lang.Object)
   */
  @Override
  public boolean add(InterestPoint e) {
    boolean inexist = exist(e) == null;

    if (inexist)
      super.add(e);

    return inexist;
  }

  /**
   * @param msg
   */
  public void add(WarbotMessage msg) {
    if (!msg.getAct().equals(Cst.Msg.IntPoint))
      return;

    WBPoint pos = new WBPoint(Double.parseDouble(msg.getArgN(2)),
        Double.parseDouble(msg.getArgN(3)));
    final InterestPoint.Type type = InterestPoint.Type.valueOf(msg.getArgN(1));

    InterestPoint ip = new InterestPoint(pos, type);
    InterestPoint toAdd = exist(ip);

    if (toAdd == null)
      super.add(ip);
  }

  /**
   * @param msg
   */
  public void del(WarbotMessage msg) {
    if (!msg.getAct().equals(Cst.Msg.delIntPoint))
      return;
    WBPoint pos = new WBPoint(Double.parseDouble(msg.getArgN(2)),
        Double.parseDouble(msg.getArgN(3)));
    final InterestPoint.Type type = InterestPoint.Type.valueOf(msg.getArgN(1));

    InterestPoint ip = new InterestPoint(pos, type);
    InterestPoint toDel = exist(ip);

    if (toDel != null)
      this.remove(toDel);
  }

  public InterestPoint getNearest(InterestPoint.Type type, WBPoint myPos) {
    if (size() == 0)
      return null;

    InterestPoint result = null;

    for (InterestPoint p : this)
      if (p.getType().equals(type)
          && (result == null || myPos.distance(p) < myPos.distance(result)))
        result = p;

    return result;
  }

  /**
   * @param food
   * @param posToRef
   * @param foes
   * @return
   */
  public InterestPoint getNearestWithoutFoe(Type type, WBPoint myPos,
      MobileFoes foes) {
    if (size() == 0)
      return null;

    InterestPoint result = null;

    for (InterestPoint p : this)
      if (p.getType().equals(type)
          && (result == null || myPos.distance(p) < myPos.distance(result))) {
        boolean isSafe = true;

        for (MobileFoe foe : foes)
          if (foe.IsFoe()
              && foe.getType().equals(MobileFoe.Type.RocketLauncher)
              && foe.getPosition().distance(p) < 80) {
            isSafe = false;
            break;
          }

        if (!isSafe)
          continue;

        result = p;
      }
    return result;
  }

  /**
   * @param perceptRadius
   *          the perceptRadius to set
   */
  public void setPerceptRadius(int perceptRadius) {
    this.perceptRadius = perceptRadius / 2;
  }

  private InterestPoint exist(InterestPoint e) {
    for (InterestPoint ip : this)
      if (e.distance(ip) < perceptRadius) {
        ip.setRefreshed(true);
        return ip;
      }
    return null;
  }
}
