package warbot.chevri_t;

import madkit.kernel.AgentAddress;

public class KnightCandidate implements Comparable<KnightCandidate>
{
  private AgentAddress agentAddr;
  private Double       score;

  public KnightCandidate(AgentAddress agentAddr, Double score) {
    this.agentAddr = agentAddr;
    this.score = score;
  }

  @Override
  public int compareTo(KnightCandidate n) {
    return -score.compareTo(n.score);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof KnightCandidate))
      return false;
    KnightCandidate n = (KnightCandidate)obj;
    return n.score == score;
  }

  /**
   * @return the Agent Address
   */
  public AgentAddress getAgentAddr() {
    return agentAddr;
  }

  /**
   * @return the Knight's score.
   */
  public double getScore() {
    return score;
  }

}
