package warbot.chevri_t;

import java.util.LinkedList;

public class Message
{
  private String   groupName;
  private String   role;
  private String   content;
  private String[] args;

  public Message(String groupName, String role, String content, String... args) {
    this.groupName = groupName;
    this.role = role;
    this.content = content;
    this.args = args;
  }

  public LinkedList<String> CompareArgsWith(Message otherMsg) {
    LinkedList<String> result = new LinkedList<String>();
    int aln = args.length;
    int oln = otherMsg.args.length;

    if (aln > oln)
      for (int i = oln; i < aln; ++i)
        result.add(args[i]);
    else if (oln > aln)
      for (int i = aln; i < oln; ++i)
        result.add(otherMsg.args[i]);
    else
      for (int i = 0; i < aln; ++i)
        if (!args[i].equals(otherMsg.args[i]))
          result.add(otherMsg.args[i]);

    return result;
  }

  /**
   * @return the args
   */
  public String[] getArgs() {
    return args;
  }

  /**
   * @return the content
   */
  public String getContent() {
    return content;
  }
  /**
   * @return the groupName
   */
  public String getGroupName() {
    return groupName;
  }
  /**
   * @return the role
   */
  public String getRole() {
    return role;
  }

  /**
   * @param args the args to set
   */
  public void setArgs(String[] args) {
    this.args = args;
  }
}
