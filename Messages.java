package warbot.chevri_t;

import java.util.HashSet;
import java.util.Hashtable;

public class Messages
{
  public enum State {
    Insert, Update, Delete, Nothing
  };

  private Hashtable<Integer, Message> messages = new Hashtable<Integer, Message>();
  private HashSet<String>             baseMsgs = new HashSet<String>();
  private int                         iter     = 0;

  public Messages() {};

  public State add(Message msg) {
    if (++iter > 100)
      iter = 0;
    String baseMsg = msg.getGroupName() + "+" + msg.getRole() + "+"
        + msg.getContent();
    int baseMsgKey = baseMsg.hashCode();

    if (baseMsgs.add(baseMsg)) {
      messages.put(baseMsgKey, msg);
      return State.Insert;
    }
    Message oldMsg = messages.get(baseMsgKey);

    if (oldMsg == null) {
      System.out.println("old Msg is null");
      return State.Nothing;
    }
    if (!oldMsg.CompareArgsWith(msg).isEmpty()) {
      oldMsg.setArgs(msg.getArgs());
      return State.Update;
    }

    if (iter > 42) {
      iter = 0;
      return State.Update;
    }

    return State.Nothing;
  }

  public boolean has(String groupName, String role, String content) {
    String baseMsg = groupName + "+" + role + "+" + content;

    return baseMsgs.contains(baseMsg);
  }

  public State remove(String groupName, String role, String content) {
    String baseMsg = groupName + "+" + role + "+" + content;
    int baseMsgKey = baseMsg.hashCode();

    if (baseMsgs.remove(baseMsg)) {
      messages.remove(baseMsgKey);
      return State.Delete;
    }

    return State.Nothing;
  }

}
