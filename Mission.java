package warbot.chevri_t;

import madkit.kernel.AgentAddress;

public class Mission implements Comparable<Mission>
{
  public enum MissionType {
    HELP(Cst.Msg.Goal.help), EXPLORE(Cst.Msg.Goal.explore), ATTACK(
        Cst.Msg.Goal.attack), RECOLT(Cst.Msg.Goal.recolt);

    private final String text;

    private MissionType(final String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  private int                tick     = 0;
  private final String       priority;
  private final AgentAddress nakamaAddress;
  private final MissionType  missionName;
  private WBPoint            location = null;

  public Mission(Mission value) {
    priority = value.priority;
    nakamaAddress = value.nakamaAddress;
    missionName = value.missionName;
    location = value.location;
  }

  public Mission(String priority, AgentAddress nakamaAddress,
      String missionName, WBPoint pos) {

    this.priority = priority;
    this.missionName = MissionType.valueOf(missionName);
    this.nakamaAddress = nakamaAddress;

    setLocation(pos);
  }

  @Override
  public int compareTo(Mission m) {
    return Cst.Priority.compareTo(priority, m.priority);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object m) {
    Mission mi = (Mission)m;
    return priority.equals(mi.priority)
        && nakamaAddress.equals(mi.nakamaAddress)
        && missionName.equals(mi.missionName) && location.equals(mi.location);
  }

  public WBPoint getLocation() {
    return location;
  }

  public String getMissionString() {
    return missionName.toString();
  }

  /**
   * @return the missionName
   */
  public MissionType getMissionType() {
    return missionName;
  }

  /**
   * @return the nakama's address
   */
  public AgentAddress getNakamaAddress() {
    return nakamaAddress;
  }

  public String getPriority() {
    return priority;
  }

  public int getTick() {
    return tick;
  }

  public Mission incrementTick() {
    tick++;
    return this;
  }

  /**
   * 
   */
  public void resetTick() {
    tick = 0;
  }

  public void setLocation(double x, double y) {
    if (location == null)
      location = new WBPoint(x, y);
    else
      location.setLocation(x, y);
  }

  public void setLocation(WBPoint location) {
    this.location = location;
  }

  @Override
  public String toString() {
    return "p:" + priority + " m:" + missionName + " l:" + location.toString();
  }

}
