/**
 * 
 */
package warbot.chevri_t;

import java.util.ArrayList;
import java.util.TreeSet;

import madkit.kernel.AgentAddress;

/**
 * @author TTD666
 * 
 */
public class Missions extends TreeSet<Mission>
{
  private static final long serialVersionUID = 2767216025793924929L;

  public void remove(AgentAddress sender) {
    ArrayList<Mission> missionsToDel = new ArrayList<Mission>();

    for (Mission m : this)
      if (m.getNakamaAddress() == sender)
        missionsToDel.add(m);

    for (Mission m : missionsToDel)
      super.remove(m);
  }

  public void remove(AgentAddress sender, String missionName) {
    ArrayList<Mission> missionsToDel = new ArrayList<Mission>();

    for (Mission m : this)
      if (m.getNakamaAddress() == sender
          && m.getMissionString().equals(missionName))
        missionsToDel.add(m);

    for (Mission m : missionsToDel)
      super.remove(m);
  }

  @Override
  public String toString() {
    String res = "";

    for (Mission m : this)
      res += " ; " + m.toString();

    return res;
  }
}
