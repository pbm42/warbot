package warbot.chevri_t;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import madkit.kernel.AgentAddress;
import warbot.chevri_t.MobileFoe.Type;
import warbot.kernel.Percept;
import warbot.kernel.WarbotMessage;

@SuppressWarnings("serial")
public abstract class Mobile extends Robot
{
  protected String                         roleName   = "mobile";
  protected String                         specificRoleName;
  protected WBPoint                        posToRef   = null;
  protected int                            nbEnnemis  = 0;
  protected HashMap<AgentAddress, WBPoint> homes      = new HashMap<AgentAddress, WBPoint>();
  protected MobileFoes                     foes       = new MobileFoes();
  protected InterestPoints                 points     = new InterestPoints();
  protected WBPoint                        target     = null;
  protected WBPoint                        tempTarget = null;
  protected Obstacles                      obst       = new Obstacles();
  protected boolean                        hasAction  = false;

  @Override
  public void activate() {
    super.activate();
    requestRole(groupName, roleName, null);
  }

  /**
   * (non-Javadoc)
   * 
   * @see warbot.chevri_t.Robot#doIt()
   */
  @Override
  public void doIt() {
    super.doIt();
    nbEnnemis = 0;
    if (posToRef == null) {
      if (compteur >= 3) {
        Say("Request pos ref");
        broadcast(groupName, "Base", Cst.Msg.posReq);
        broadcast(groupName, "mobile", Cst.Msg.posReq);
      }
    } else if (getCoveredDistance() > 0)
      posToRef.updatePos(getHeading(), getCoveredDistance());

    if (missions.isEmpty() && posToRef != null) {
      Say("Request mission");
      broadcast(groupName, "Base", Cst.Msg.goalReq,
          new String[] { specificRoleName });
      broadcast(groupName, "mobile", Cst.Msg.goalReq,
          new String[] { specificRoleName });
    }
  }

  @Override
  public void end() {
    broadcast(groupName, "mobile", Cst.Msg.dead, String.valueOf(nbEnnemis));
    broadcast(groupName, "Base", Cst.Msg.dead);
  }

  protected Boolean CommonMessages(WarbotMessage msg) {
    if (msg.getAct().equals(Cst.Msg.dead)) {
      missions.remove(msg.getSender());
      homes.remove(msg.getSender());
      toDoWhenDead(msg.getSender());
    } else if (msg.getAct().equals(Cst.Msg.cancel))
      missions.remove(msg.getSender(), msg.getArg1());
    else if (msg.getAct().equals(Cst.Msg.ko))
      missions.remove(msg.getSender(), msg.getArg1());
    else if (msg.getAct().equals(Cst.Msg.ping))
      send(msg.getSender(), Cst.Msg.pong, "Mobile");
    else if (msg.getAct().equals(Cst.Msg.foe))
      foes.put(msg);
    else if (msg.getAct().equals(Cst.Msg.IntPoint))
      points.add(msg);
    else if (msg.getAct().equals(Cst.Msg.delFoe))
      foes.del(msg);
    else if (msg.getAct().equals(Cst.Msg.delIntPoint))
      points.del(msg);
    else if (msg.getAct().equals(Cst.Msg.refreshMission)) {
      if (msg.getArg1().equals(Cst.Msg.Goal.help))
        if (!foeNear && !estAttaquee)
          send(msg.getSender(), Cst.Msg.cancel, Cst.Msg.Goal.help);
    }

    else
      return false;
    return true;
  }

  protected double getDeltaDir(double dir, double delta) {
    double res = dir + delta;

    if (res < 0) {
      double ft = Math.ceil(-res / 360);
      res = 360 + ft * res;
    }

    while (res >= 360)
      res %= 360;

    assert res >= 360 || res < 0;
    return res;
  }

  protected boolean isNearABase(Point2D.Double pos) {
    for (Entry<AgentAddress, WBPoint> homePos : homes.entrySet())
      if (homePos.getValue().distance(pos) < 42)
        return true;

    return false;
  }

  /**
   * 
   * @param msg
   *          : warbot message
   * @return true if msg handled.
   */
  protected Boolean MessageRefPos(WarbotMessage msg) {
    if (msg.getAct().equals(Cst.Msg.posRep)) {
      if (posToRef == null)
        posToRef = new WBPoint(-msg.getFromX()
            + Double.parseDouble(msg.getArg1()), -msg.getFromY()
            + Double.parseDouble(msg.getArg2()));
    } else if (msg.getAct().equals(Cst.Msg.posReq) && posToRef != null)
      send(msg.getSender(), Cst.Msg.posRep, posToRef.toStrings());
    else
      return false;
    return true;
  }

  protected void SendHelp() {
    WBPoint pos = foeAttackPos == null ? posToRef : foeAttackPos;
    String[] args = new String[] { Cst.Priority.High, Double.toString(pos.x),
        Double.toString(pos.y) };

    if (getShot())
      Messenger(groupName, "Launcher", Cst.Msg.Goal.help, args);

    if (!foeNear && !estAttaquee)
      Messenger(groupName, "Launcher", Cst.Msg.cancel, Cst.Msg.Goal.help);
  }

  protected void SendMessages() {
    for (MobileFoe m : foes)
      if (m.IsFoe() && !m.getType().equals(Type.Rocket) && m.IsPercepted())
        broadcast(groupName, "mobile", Cst.Msg.foe, m.toStrings());

    for (InterestPoint p : points)
      if (p.isNew()) {
        Messenger(groupName, "mobile", Cst.Msg.IntPoint, p.toStrings());
        p.setOld();
      }

    SendHelp();
  }

  /**
   * @param agentAddress
   *          Address of the dead;
   * 
   */
  protected void toDoWhenDead(AgentAddress agentAddress) {
    if (agentAddress == null)
      Say("Agent Address of the dead is dead");
    return;
  }

  protected WBPoint toWBPoint(Percept p) {
    if (posToRef == null)
      return null;
    return new WBPoint(p.getX() + posToRef.getX(), p.getY() + posToRef.getY());
  }

  protected WBPoint toWBPoint(WarbotMessage m) {
    if (posToRef == null)
      return null;

    return new WBPoint(m.getFromX() + posToRef.getX(), m.getFromY()
        + posToRef.getY());
  }

  @Deprecated
  protected WBPoint toWBPointInvers(WarbotMessage m) {
    if (posToRef == null)
      return null;

    WBPoint allyPos = new WBPoint(m.getFromX(), m.getFromY());
    Say("Ally pos relativ", allyPos.toString(), "relativ pos",
        posToRef.toString());
    return new WBPoint(m.getFromX() + posToRef.getX(), m.getFromY()
        + posToRef.getY());
  }

  protected void UpdateFoesAndIP() {
    ArrayList<MobileFoe> toDel = new ArrayList<MobileFoe>();
    ArrayList<InterestPoint> toDelIP = new ArrayList<InterestPoint>();

    int maxDist = specificRoleName.equals("Explorer") ? Cst.DR.ExplorerDR
        : Cst.DR.LauncherDR;
    for (MobileFoe set : foes) {
      if (set.getTick() > 20 || !set.getRefreshed()
          && set.getDistance(posToRef) < maxDist - 5)
        toDel.add(set);
      set.setRefreshed(false);
    }

    for (InterestPoint ip : points) {
      if (!ip.isRefreshed() && ip.distance(posToRef) < maxDist - 10)
        toDelIP.add(ip);
      ip.setRefreshed(false);
    }

    for (MobileFoe key : toDel) {
      broadcast(groupName, "mobile", Cst.Msg.delFoe, key.toStrings());
      foes.remove(key);
    }

    for (InterestPoint ip : toDelIP) {
      broadcast(groupName, "mobile", Cst.Msg.delIntPoint, ip.toStrings());
      points.remove(ip);
    }
  }
}
