package warbot.chevri_t;

import java.awt.geom.Point2D;

public class MobileFoe
{
  enum Type {
    Explorer("Explorer"), RocketLauncher("RocketLauncher"), Rocket("Rocket"), Default(
        "Default");

    private final String value;

    private Type(final String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return value;
    }
  };

  private final double  CD;

  private final int     MAX_PAUSE_TICK = 5;
  private int           curPauseTick   = MAX_PAUSE_TICK;
  private int           tick           = 0;
  private final Type    type;
  private WBPoint       pos;
  private int           hp;
  private double        direction      = -1;
  private Boolean       isPercepted    = false;
  private Boolean       refreshed      = true;
  private double        radius         = 18;
  private final Boolean isFoe;
  private final int     perceptRadius;

  public MobileFoe(Type type, double x, double y, int hp, Boolean isFoe,
      int perceptRad) {
    switch (type) {
    case Default:
      CD = 0;
      radius = 26;
      break;
    case Explorer:
      CD = Cst.CD.ExplorerCD;
      break;
    case RocketLauncher:
      CD = Cst.CD.LauncherCD;
      break;
    case Rocket:
      CD = Cst.CD.RocketCD;
      break;
    default:
      CD = 0;
      break;
    }

    this.type = type;
    pos = new WBPoint(x, y);
    this.hp = hp;
    this.isFoe = isFoe;
    perceptRadius = perceptRad;
  }

  /**
   * 
   * @return The MobileFoe's maximum covered distance.
   */
  public double getCD() {
    return CD;
  }

  /**
   * @return the direction
   */
  public double getDirection() {
    return direction;
  }

  /**
   * @return the distance
   */
  public double getDistance(Point2D.Double posRef) {
    return pos.distance(posRef);
  }

  /**
   * @return the hp
   */
  public int getHitPoint() {
    return hp;
  }

  public int getPerceptRadius() {
    return perceptRadius;
  }

  /**
   * @return the pos
   */
  public WBPoint getPosition() {
    return pos;
  }

  public double getRadius() {
    return radius;
  }

  /**
   * @return the refreshed
   */
  public Boolean getRefreshed() {
    return refreshed;
  }

  public WBPoint getTargetPoint(double multiplicator) {
    return pos.getTargetPoint(multiplicator, direction);
  }

  public int getTick() {
    return tick;
  }

  /**
   * @return the type
   */
  public Type getType() {
    return type;
  }

  public Point2D getVelocityVector() {
    if (direction == -1)
      return new WBPoint(0, 0);
    double vx = Math.cos(Math.toRadians(direction));
    double vy = Math.sin(Math.toRadians(direction));

    return new WBPoint(vx * CD, vy * CD);
  }

  /**
   * @param posToRef
   * @return
   */
  public boolean isApproaching(WBPoint posToRef) {

    double initialDistance = pos.distance(posToRef);
    double finalDistance = getTargetPoint(2).distance(posToRef);

    return finalDistance < initialDistance;
  }

  public boolean IsFoe() {
    return isFoe;
  }

  public Boolean IsPercepted() {
    return isPercepted;
  }

  public void IsPercepted(boolean is) {
    isPercepted = is;
  }

  public void resetTick() {
    tick = 0;
  }

  public void setDirection(double dir) {
    direction = dir;
  }

  /**
   * @param x
   *          the new pos.x to set
   * @param y
   *          the new pos.y to set
   */
  public void setDirection(double x, double y) {
    double newDir = -1;
    if (pos.equals(x, y, 0.8)) {
      if (--curPauseTick <= 0)
        curPauseTick = MAX_PAUSE_TICK;
      else
        return;

    } else
      newDir = pos.direction(x, y);
    direction = newDir;
  }

  /**
   * @param hp
   *          the hp to set
   */
  public void setHitPoint(int hp) {
    this.hp = hp;
  }

  /**
   * @param pos
   *          the pos to set
   */
  public void setPosition(WBPoint pos) {
    this.pos = pos;
  }

  /**
   * @param refreshed
   *          the refreshed to set
   */
  public void setRefreshed(Boolean refreshed) {
    tick++;
    this.refreshed = refreshed;
  }

  public String[] toStrings() {
    return new String[] { Double.toString(pos.x), Double.toString(pos.y),
        Double.toString(direction), Integer.toString(hp), type.toString(),
        Integer.toString(perceptRadius) };
  }

}
