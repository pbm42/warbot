/**
 * 
 */
package warbot.chevri_t;

import java.awt.geom.Point2D;
import java.util.LinkedList;

import warbot.chevri_t.MobileFoe.Type;
import warbot.kernel.Percept;
import warbot.kernel.WarbotMessage;

/**
 * @author TTD666
 * 
 */
@SuppressWarnings("serial")
public class MobileFoes extends LinkedList<MobileFoe>
{

  /**
   * @param msg
   *          Message containing information about the foe to delete.
   */
  public void del(WarbotMessage msg) {
    if (!msg.getAct().equals(Cst.Msg.delFoe))
      return;
    Point2D.Double pos = new Point2D.Double(Double.parseDouble(msg.getArgN(1)),
        Double.parseDouble(msg.getArgN(2)));
    final MobileFoe.Type type = MobileFoe.Type.valueOf(msg.getArgN(5));

    MobileFoe toDel = null;

    for (MobileFoe mf : this)
      if (mf.getPosition().equals(pos) && mf.getType().equals(type)) {
        toDel = mf;
        break;
      }
    if (toDel != null)
      this.remove(toDel);
  }

  public MobileFoe put(final Percept e, final WBPoint posToRef, Boolean isFoe,
      int perceptRad) {

    MobileFoe foe = null;
    double x = e.getX() + posToRef.getX();
    double y = e.getY() + posToRef.getY();
    final Type type;
    Type preType;

    try {
      preType = Type.valueOf(e.getPerceptType());
    }
    catch (Exception ex) {
      preType = Type.Default;
    }
    type = preType;

    foe = exist(x, y, e.getEnergy(), type);

    if (foe == null) {
      foe = new MobileFoe(type, x, y, e.getEnergy(), isFoe, perceptRad);
      super.add(foe);
    }
    foe.resetTick();
    foe.IsPercepted(true);

    return foe;
  }

  public MobileFoe put(WarbotMessage msg) {
    if (!msg.getAct().equals(Cst.Msg.foe))
      return null;

    Point2D.Double pos = new Point2D.Double(Double.parseDouble(msg.getArgN(1)),
        Double.parseDouble(msg.getArgN(2)));
    double dir = Double.parseDouble(msg.getArgN(3));
    int hp = Integer.parseInt(msg.getArgN(4));
    MobileFoe.Type type = MobileFoe.Type.valueOf(msg.getArgN(5));
    int perceptRad = Integer.parseInt(msg.getArgN(6));

    MobileFoe newFoe = exist(pos.x, pos.y, hp, type);

    if (newFoe == null) {
      newFoe = new MobileFoe(type, pos.x, pos.y, hp, true, perceptRad);
      newFoe.IsPercepted(false);
      newFoe.setDirection(dir);
      this.add(newFoe);
    }

    return newFoe;
  }

  private MobileFoe exist(double x, double y, int energy, Type type) {
    for (MobileFoe f : this) {
      double distance = f.getPosition().distance(x, y);
      if (distance < 10 && f.getType().equals(type)) {
        f.setHitPoint(energy);
        f.setRefreshed(true);
        f.setDirection(x, y);
        f.setPosition(new WBPoint(x, y));
        return f;
      }
    }
    return null;
  }
}
