/**
 * 
 */
package warbot.chevri_t;

/**
 * @author chevri_t
 * 
 */
public class Obstacle
{
  WBPoint position;
  double  radius = 12;

  public Obstacle(WBPoint wbPoint, int radius) {
    position = wbPoint;
    this.radius = radius;
  }

  @Override
  public boolean equals(Object ob) {
    if (ob == null)
      return false;

    Obstacle obt = (Obstacle)ob;

    return obt.position.equals(position) && obt.radius == radius;

  }

  public WBPoint getPos() {
    return position;
  }

  public double getRadius() {
    return radius;
  }

}
