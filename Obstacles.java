/**
 * 
 */
package warbot.chevri_t;

import java.util.ArrayList;

/**
 * @author chevri_t
 * 
 */
@SuppressWarnings("serial")
public class Obstacles extends ArrayList<Obstacle>
{
  @Override
  public boolean add(Obstacle ob) {
    if (contains(ob))
      return false;
    else
      super.add(ob);
    return true;
  }
}
