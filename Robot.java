package warbot.chevri_t;

import java.util.LinkedList;

import warbot.chevri_t.Messages.State;
import warbot.kernel.Brain;

@SuppressWarnings("serial")
public abstract class Robot extends Brain
{
  protected String              groupName    = "warbot-chevri_t-";
  protected Boolean             foeNear      = false;
  protected Boolean             estAttaquee  = false;
  protected Messages            messages     = new Messages();
  protected LinkedList<Boolean> shots        = new LinkedList<Boolean>();
  protected Missions            missions     = new Missions();
  protected int                 compteur     = 0;
  protected int                 baseEnergyLevel;
  protected WBPoint             foeAttackPos = null;

  @Override
  public void activate() {
    groupName = groupName + getTeam();
    createGroup(false, groupName, null, null);
    requestRole(groupName, "Robot", null);
    showUserMessage(true);
    baseEnergyLevel = getEnergyLevel();
  }

  @Override
  public void doIt() {
    SayToUI("");
    foeAttackPos = null;
    if (compteur < Integer.MAX_VALUE)
      compteur++;

    MajEstAttaquee();
    foeNear = estAttaquee;

    CheckMessages();
    ManageMissions();
  }

  @Override
  abstract public void end();

  protected abstract void CheckMessages();

  protected double DefinePriority(double k_x, double k_y, int k_hp) {
    double distance = Math.sqrt(k_x * k_x + k_y * k_y);

    return distance / k_hp;
  }

  protected void MajEstAttaquee() {
    shots.add(getShot());

    if (shots.size() > 5)
      shots.remove();

    estAttaquee = shots.contains(true);
  }

  abstract protected void ManageMissions();

  protected void Messenger(String groupName, String role, String content,
      String... arg) {

    if (content.equals(Cst.Msg.cancel)) {
      if (messages.remove(groupName, role, arg[0]).equals(State.Delete))
        // Say("Annulation", arg[0]);
        broadcast(groupName, role, content, arg);
      return;
    }

    Message newMsg = new Message(groupName, role, content, arg);

    Messages.State state = messages.add(newMsg);
    if (state.equals(State.Insert))
      broadcast(groupName, role, content, arg);
    // Say("Envoi", content, arg[0]);
    else if (state.equals(State.Update))
      broadcast(groupName, role, content, arg);
    // Say("Envoi", content, arg[0]);
  }

  protected void Say(String... strings) {
    String message = "";

    for (String m : strings)
      message += m + "; ";

    SayToConsol(message);
    setUserMessage(message);
  }

  protected void SayToConsol(String message) {
    System.out.println(getTeam() + " : " + message);
  }

  protected void SayToUI(String... strings) {
    String message = "";

    for (String m : strings)
      message += m + "; ";

    setUserMessage(message);

  }

}
