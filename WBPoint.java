package warbot.chevri_t;

import java.awt.geom.Point2D;

@SuppressWarnings("serial")
public class WBPoint extends Point2D.Double
{
  public WBPoint(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public WBPoint(Point2D.Double point) {
    x = point.x;
    y = point.y;
  }

  public double direction(double x, double y) {

    double a = x - getX();
    double b = y - getY();

    if (equals(x, y, 0.8))
      return -1.0;

    if (b < 0)
      return 180 * Math.asin(a / Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)))
          / Math.PI + 270;
    else
      return 180 * Math.acos(a / Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2)))
          / Math.PI;
  }

  public double direction(Point2D point) {
    return direction(point.getX(), point.getY());
  }

  public boolean equals(double x, double y) {
    return Math.abs(this.x - x) < 2 && Math.abs(this.y - y) < 2;
  }

  public boolean equals(double x, double y, double accur) {
    return Math.abs(this.x - x) < accur && Math.abs(this.y - y) < accur;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null)
      return false;

    Point2D.Double point = (Point2D.Double)o;
    return Math.abs(point.x - x) < 1 && Math.abs(point.y - y) < 1;
  }

  public WBPoint getTargetPoint(double multiplicator, double direction) {
    if (direction == -1)
      return null;
    double nx = Math.cos(Math.toRadians(direction));
    double ny = Math.sin(Math.toRadians(direction));

    return new WBPoint(nx * multiplicator + x, ny * multiplicator + y);
  }

  @Override
  public String toString() {
    return "[" + java.lang.Double.toString(x) + " ; "
        + java.lang.Double.toString(y) + "]";
  }

  public String[] toStrings() {
    return new String[] { java.lang.Double.toString(x),
        java.lang.Double.toString(y) };
  }

  public void updatePos(double heading, int coveredDistance) {
    double uX = Math.cos(Math.toRadians(heading)) * coveredDistance + x;
    double uY = Math.sin(Math.toRadians(heading)) * coveredDistance + y;

    setLocation(uX, uY);
  }
}
